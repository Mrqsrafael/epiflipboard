{
	"id": 0,
	"name": "reftis-api",
	"kind": 0,
	"flags": {},
	"originalName": "",
	"children": [
		{
			"id": 110,
			"name": "\"app\"",
			"kind": 1,
			"kindString": "Module",
			"flags": {
				"isExported": true
			},
			"originalName": "/home/mrafael/PERSO/reftis-api/src/app.ts",
			"children": [
				{
					"id": 111,
					"name": "app",
					"kind": 32,
					"kindString": "Variable",
					"flags": {
						"isExported": true,
						"isConst": true
					},
					"sources": [
						{
							"fileName": "app.ts",
							"line": 13,
							"character": 9
						}
					],
					"type": {
						"type": "reference",
						"name": "Express"
					},
					"defaultValue": "express()"
				}
			],
			"groups": [
				{
					"title": "Variables",
					"kind": 32,
					"children": [
						111
					]
				}
			],
			"sources": [
				{
					"fileName": "app.ts",
					"line": 1,
					"character": 0
				}
			]
		},
		{
			"id": 112,
			"name": "\"index\"",
			"kind": 1,
			"kindString": "Module",
			"flags": {
				"isExported": true
			},
			"originalName": "/home/mrafael/PERSO/reftis-api/src/index.ts",
			"sources": [
				{
					"fileName": "index.ts",
					"line": 1,
					"character": 0
				}
			]
		},
		{
			"id": 113,
			"name": "\"server\"",
			"kind": 1,
			"kindString": "Module",
			"flags": {
				"isExported": true
			},
			"originalName": "/home/mrafael/PERSO/reftis-api/src/server.ts",
			"children": [
				{
					"id": 114,
					"name": "server",
					"kind": 32,
					"kindString": "Variable",
					"flags": {
						"isExported": true,
						"isConst": true
					},
					"sources": [
						{
							"fileName": "server.ts",
							"line": 7,
							"character": 12
						}
					],
					"type": {
						"type": "reference",
						"name": "Server"
					},
					"defaultValue": "app.listen(app.get(\"port\"), () => {\n    console.log(` App is running at http://localhost:${app.get(\"port\")} in ${app.get(\"env\")} mode`);\n    console.log(\" Press CTRL-C to stop\\n\");\n})"
				}
			],
			"groups": [
				{
					"title": "Variables",
					"kind": 32,
					"children": [
						114
					]
				}
			],
			"sources": [
				{
					"fileName": "server.ts",
					"line": 1,
					"character": 0
				}
			]
		},
		{
			"id": 55,
			"name": "\"services/auth/auth\"",
			"kind": 1,
			"kindString": "Module",
			"flags": {
				"isExported": true
			},
			"originalName": "/home/mrafael/PERSO/reftis-api/src/services/auth/auth.ts",
			"children": [
				{
					"id": 56,
					"name": "jwtKey",
					"kind": 32,
					"kindString": "Variable",
					"flags": {
						"isConst": true
					},
					"sources": [
						{
							"fileName": "services/auth/auth.ts",
							"line": 4,
							"character": 12
						}
					],
					"type": {
						"type": "union",
						"types": [
							{
								"type": "intrinsic",
								"name": "undefined"
							},
							{
								"type": "intrinsic",
								"name": "string"
							}
						]
					},
					"defaultValue": "process.env[\"JWT_KEY\"]"
				},
				{
					"id": 57,
					"name": "generateAuthToken",
					"kind": 64,
					"kindString": "Function",
					"flags": {
						"isExported": true
					},
					"signatures": [
						{
							"id": 58,
							"name": "generateAuthToken",
							"kind": 4096,
							"kindString": "Call signature",
							"flags": {
								"isExported": true
							},
							"parameters": [
								{
									"id": 59,
									"name": "user",
									"kind": 32768,
									"kindString": "Parameter",
									"flags": {
										"isExported": true
									},
									"type": {
										"type": "reference",
										"id": 26,
										"name": "User"
									}
								}
							],
							"type": {
								"type": "union",
								"types": [
									{
										"type": "intrinsic",
										"name": "string"
									},
									{
										"type": "reference",
										"name": "Error"
									}
								]
							}
						}
					],
					"sources": [
						{
							"fileName": "services/auth/auth.ts",
							"line": 6,
							"character": 33
						}
					]
				}
			],
			"groups": [
				{
					"title": "Variables",
					"kind": 32,
					"children": [
						56
					]
				},
				{
					"title": "Functions",
					"kind": 64,
					"children": [
						57
					]
				}
			],
			"sources": [
				{
					"fileName": "services/auth/auth.ts",
					"line": 1,
					"character": 0
				}
			]
		},
		{
			"id": 75,
			"name": "\"services/auth/middlewares\"",
			"kind": 1,
			"kindString": "Module",
			"flags": {
				"isExported": true
			},
			"originalName": "/home/mrafael/PERSO/reftis-api/src/services/auth/middlewares.ts",
			"children": [
				{
					"id": 94,
					"name": "adminAuthMiddlewares",
					"kind": 32,
					"kindString": "Variable",
					"flags": {
						"isExported": true,
						"isConst": true
					},
					"sources": [
						{
							"fileName": "services/auth/middlewares.ts",
							"line": 77,
							"character": 33
						}
					],
					"type": {
						"type": "array",
						"elementType": {
							"type": "union",
							"types": [
								{
									"type": "reference",
									"id": 78,
									"name": "extractToken"
								},
								{
									"type": "reference",
									"id": 88,
									"name": "adminRequired"
								}
							]
						}
					},
					"defaultValue": "[extractToken, authenticateToken, adminRequired]"
				},
				{
					"id": 76,
					"name": "jwtKey",
					"kind": 32,
					"kindString": "Variable",
					"flags": {
						"isConst": true
					},
					"sources": [
						{
							"fileName": "services/auth/middlewares.ts",
							"line": 9,
							"character": 12
						}
					],
					"type": {
						"type": "union",
						"types": [
							{
								"type": "intrinsic",
								"name": "undefined"
							},
							{
								"type": "intrinsic",
								"name": "string"
							}
						]
					},
					"defaultValue": "process.env[\"JWT_KEY\"]"
				},
				{
					"id": 77,
					"name": "permit",
					"kind": 32,
					"kindString": "Variable",
					"flags": {
						"isConst": true
					},
					"sources": [
						{
							"fileName": "services/auth/middlewares.ts",
							"line": 10,
							"character": 12
						}
					],
					"type": {
						"type": "reference",
						"name": "Bearer"
					},
					"defaultValue": "new Bearer({\n    query: \"access_token\", // Also allow an `?access_token=` query parameter.\n})"
				},
				{
					"id": 93,
					"name": "userAuthMiddlewares",
					"kind": 32,
					"kindString": "Variable",
					"flags": {
						"isExported": true,
						"isConst": true
					},
					"sources": [
						{
							"fileName": "services/auth/middlewares.ts",
							"line": 75,
							"character": 32
						}
					],
					"type": {
						"type": "array",
						"elementType": {
							"type": "union",
							"types": [
								{
									"type": "reference",
									"id": 78,
									"name": "extractToken"
								},
								{
									"type": "reference",
									"id": 83,
									"name": "authenticateToken"
								}
							]
						}
					},
					"defaultValue": "[extractToken, authenticateToken]"
				},
				{
					"id": 88,
					"name": "adminRequired",
					"kind": 64,
					"kindString": "Function",
					"flags": {
						"isExported": true
					},
					"signatures": [
						{
							"id": 89,
							"name": "adminRequired",
							"kind": 4096,
							"kindString": "Call signature",
							"flags": {
								"isExported": true
							},
							"parameters": [
								{
									"id": 90,
									"name": "req",
									"kind": 32768,
									"kindString": "Parameter",
									"flags": {
										"isExported": true
									},
									"type": {
										"type": "reference",
										"name": "Request"
									}
								},
								{
									"id": 91,
									"name": "res",
									"kind": 32768,
									"kindString": "Parameter",
									"flags": {
										"isExported": true
									},
									"type": {
										"type": "reference",
										"name": "Response"
									}
								},
								{
									"id": 92,
									"name": "next",
									"kind": 32768,
									"kindString": "Parameter",
									"flags": {
										"isExported": true
									},
									"type": {
										"type": "reference",
										"name": "NextFunction"
									}
								}
							],
							"type": {
								"type": "reference",
								"typeArguments": [
									{
										"type": "union",
										"types": [
											{
												"type": "intrinsic",
												"name": "void"
											},
											{
												"type": "reference",
												"typeArguments": [
													{
														"type": "intrinsic",
														"name": "any"
													}
												],
												"name": "Response"
											}
										]
									}
								],
								"name": "Promise"
							}
						}
					],
					"sources": [
						{
							"fileName": "services/auth/middlewares.ts",
							"line": 66,
							"character": 35
						}
					]
				},
				{
					"id": 83,
					"name": "authenticateToken",
					"kind": 64,
					"kindString": "Function",
					"flags": {
						"isExported": true
					},
					"signatures": [
						{
							"id": 84,
							"name": "authenticateToken",
							"kind": 4096,
							"kindString": "Call signature",
							"flags": {
								"isExported": true
							},
							"parameters": [
								{
									"id": 85,
									"name": "req",
									"kind": 32768,
									"kindString": "Parameter",
									"flags": {
										"isExported": true
									},
									"type": {
										"type": "reference",
										"name": "Request"
									}
								},
								{
									"id": 86,
									"name": "res",
									"kind": 32768,
									"kindString": "Parameter",
									"flags": {
										"isExported": true
									},
									"type": {
										"type": "reference",
										"name": "Response"
									}
								},
								{
									"id": 87,
									"name": "next",
									"kind": 32768,
									"kindString": "Parameter",
									"flags": {
										"isExported": true
									},
									"type": {
										"type": "reference",
										"name": "NextFunction"
									}
								}
							],
							"type": {
								"type": "reference",
								"typeArguments": [
									{
										"type": "intrinsic",
										"name": "void"
									}
								],
								"name": "Promise"
							}
						}
					],
					"sources": [
						{
							"fileName": "services/auth/middlewares.ts",
							"line": 30,
							"character": 39
						}
					]
				},
				{
					"id": 78,
					"name": "extractToken",
					"kind": 64,
					"kindString": "Function",
					"flags": {
						"isExported": true
					},
					"signatures": [
						{
							"id": 79,
							"name": "extractToken",
							"kind": 4096,
							"kindString": "Call signature",
							"flags": {
								"isExported": true
							},
							"parameters": [
								{
									"id": 80,
									"name": "req",
									"kind": 32768,
									"kindString": "Parameter",
									"flags": {
										"isExported": true
									},
									"type": {
										"type": "reference",
										"name": "Request"
									}
								},
								{
									"id": 81,
									"name": "res",
									"kind": 32768,
									"kindString": "Parameter",
									"flags": {
										"isExported": true
									},
									"type": {
										"type": "reference",
										"name": "Response"
									}
								},
								{
									"id": 82,
									"name": "next",
									"kind": 32768,
									"kindString": "Parameter",
									"flags": {
										"isExported": true
									},
									"type": {
										"type": "reference",
										"name": "NextFunction"
									}
								}
							],
							"type": {
								"type": "union",
								"types": [
									{
										"type": "intrinsic",
										"name": "void"
									},
									{
										"type": "reference",
										"name": "Error"
									}
								]
							}
						}
					],
					"sources": [
						{
							"fileName": "services/auth/middlewares.ts",
							"line": 14,
							"character": 28
						}
					]
				}
			],
			"groups": [
				{
					"title": "Variables",
					"kind": 32,
					"children": [
						94,
						76,
						77,
						93
					]
				},
				{
					"title": "Functions",
					"kind": 64,
					"children": [
						88,
						83,
						78
					]
				}
			],
			"sources": [
				{
					"fileName": "services/auth/middlewares.ts",
					"line": 1,
					"character": 0
				}
			]
		},
		{
			"id": 108,
			"name": "\"services/auth/routes\"",
			"kind": 1,
			"kindString": "Module",
			"flags": {
				"isExported": true
			},
			"originalName": "/home/mrafael/PERSO/reftis-api/src/services/auth/routes.ts",
			"children": [
				{
					"id": 109,
					"name": "router",
					"kind": 32,
					"kindString": "Variable",
					"flags": {
						"isConst": true
					},
					"sources": [
						{
							"fileName": "services/auth/routes.ts",
							"line": 6,
							"character": 12
						}
					],
					"type": {
						"type": "reference",
						"name": "Router"
					},
					"defaultValue": "Router()"
				}
			],
			"groups": [
				{
					"title": "Variables",
					"kind": 32,
					"children": [
						109
					]
				}
			],
			"sources": [
				{
					"fileName": "services/auth/routes.ts",
					"line": 1,
					"character": 0
				}
			]
		},
		{
			"id": 33,
			"name": "\"services/user/controller\"",
			"kind": 1,
			"kindString": "Module",
			"flags": {
				"isExported": true
			},
			"originalName": "/home/mrafael/PERSO/reftis-api/src/services/user/controller.ts",
			"children": [
				{
					"id": 34,
					"name": "prisma",
					"kind": 32,
					"kindString": "Variable",
					"flags": {
						"isConst": true
					},
					"sources": [
						{
							"fileName": "services/user/controller.ts",
							"line": 5,
							"character": 12
						}
					],
					"type": {
						"type": "reference",
						"typeArguments": [
							{
								"type": "reflection",
								"declaration": {
									"id": 35,
									"name": "__type",
									"kind": 65536,
									"kindString": "Type literal",
									"flags": {}
								}
							},
							{
								"type": "intrinsic",
								"name": "never"
							}
						],
						"name": "PrismaClient"
					},
					"defaultValue": "new PrismaClient()"
				},
				{
					"id": 47,
					"name": "create",
					"kind": 64,
					"kindString": "Function",
					"flags": {
						"isExported": true
					},
					"signatures": [
						{
							"id": 48,
							"name": "create",
							"kind": 4096,
							"kindString": "Call signature",
							"flags": {
								"isExported": true
							},
							"parameters": [
								{
									"id": 49,
									"name": "email",
									"kind": 32768,
									"kindString": "Parameter",
									"flags": {
										"isExported": true
									},
									"type": {
										"type": "intrinsic",
										"name": "string"
									}
								},
								{
									"id": 50,
									"name": "username",
									"kind": 32768,
									"kindString": "Parameter",
									"flags": {
										"isExported": true
									},
									"type": {
										"type": "intrinsic",
										"name": "string"
									}
								},
								{
									"id": 51,
									"name": "password",
									"kind": 32768,
									"kindString": "Parameter",
									"flags": {
										"isExported": true
									},
									"type": {
										"type": "intrinsic",
										"name": "string"
									}
								}
							],
							"type": {
								"type": "reference",
								"typeArguments": [
									{
										"type": "reference",
										"id": 26,
										"name": "User"
									}
								],
								"name": "Promise"
							}
						}
					],
					"sources": [
						{
							"fileName": "services/user/controller.ts",
							"line": 35,
							"character": 28
						}
					]
				},
				{
					"id": 52,
					"name": "deleteByID",
					"kind": 64,
					"kindString": "Function",
					"flags": {
						"isExported": true
					},
					"signatures": [
						{
							"id": 53,
							"name": "deleteByID",
							"kind": 4096,
							"kindString": "Call signature",
							"flags": {
								"isExported": true
							},
							"parameters": [
								{
									"id": 54,
									"name": "id",
									"kind": 32768,
									"kindString": "Parameter",
									"flags": {
										"isExported": true
									},
									"type": {
										"type": "intrinsic",
										"name": "string"
									}
								}
							],
							"type": {
								"type": "reference",
								"typeArguments": [
									{
										"type": "reference",
										"id": 26,
										"name": "User"
									}
								],
								"name": "Promise"
							}
						}
					],
					"sources": [
						{
							"fileName": "services/user/controller.ts",
							"line": 46,
							"character": 32
						}
					]
				},
				{
					"id": 36,
					"name": "getAll",
					"kind": 64,
					"kindString": "Function",
					"flags": {
						"isExported": true
					},
					"signatures": [
						{
							"id": 37,
							"name": "getAll",
							"kind": 4096,
							"kindString": "Call signature",
							"flags": {
								"isExported": true
							},
							"type": {
								"type": "reference",
								"typeArguments": [
									{
										"type": "array",
										"elementType": {
											"type": "reference",
											"typeArguments": [
												{
													"type": "intrinsic",
													"name": "string"
												},
												{
													"type": "intrinsic",
													"name": "any"
												}
											],
											"name": "Record"
										}
									}
								],
								"name": "Promise"
							}
						}
					],
					"sources": [
						{
							"fileName": "services/user/controller.ts",
							"line": 7,
							"character": 28
						}
					]
				},
				{
					"id": 44,
					"name": "getByEmail",
					"kind": 64,
					"kindString": "Function",
					"flags": {
						"isExported": true
					},
					"signatures": [
						{
							"id": 45,
							"name": "getByEmail",
							"kind": 4096,
							"kindString": "Call signature",
							"flags": {
								"isExported": true
							},
							"parameters": [
								{
									"id": 46,
									"name": "email",
									"kind": 32768,
									"kindString": "Parameter",
									"flags": {
										"isExported": true
									},
									"type": {
										"type": "intrinsic",
										"name": "string"
									}
								}
							],
							"type": {
								"type": "reference",
								"typeArguments": [
									{
										"type": "union",
										"types": [
											{
												"type": "reference",
												"id": 26,
												"name": "User"
											},
											{
												"type": "intrinsic",
												"name": "null"
											}
										]
									}
								],
								"name": "Promise"
							}
						}
					],
					"sources": [
						{
							"fileName": "services/user/controller.ts",
							"line": 27,
							"character": 32
						}
					]
				},
				{
					"id": 41,
					"name": "getByID",
					"kind": 64,
					"kindString": "Function",
					"flags": {
						"isExported": true
					},
					"signatures": [
						{
							"id": 42,
							"name": "getByID",
							"kind": 4096,
							"kindString": "Call signature",
							"flags": {
								"isExported": true
							},
							"parameters": [
								{
									"id": 43,
									"name": "id",
									"kind": 32768,
									"kindString": "Parameter",
									"flags": {
										"isExported": true
									},
									"type": {
										"type": "intrinsic",
										"name": "string"
									}
								}
							],
							"type": {
								"type": "reference",
								"typeArguments": [
									{
										"type": "union",
										"types": [
											{
												"type": "reference",
												"id": 26,
												"name": "User"
											},
											{
												"type": "intrinsic",
												"name": "null"
											}
										]
									}
								],
								"name": "Promise"
							}
						}
					],
					"sources": [
						{
							"fileName": "services/user/controller.ts",
							"line": 19,
							"character": 29
						}
					]
				},
				{
					"id": 38,
					"name": "getByUsername",
					"kind": 64,
					"kindString": "Function",
					"flags": {
						"isExported": true
					},
					"signatures": [
						{
							"id": 39,
							"name": "getByUsername",
							"kind": 4096,
							"kindString": "Call signature",
							"flags": {
								"isExported": true
							},
							"parameters": [
								{
									"id": 40,
									"name": "username",
									"kind": 32768,
									"kindString": "Parameter",
									"flags": {
										"isExported": true
									},
									"type": {
										"type": "intrinsic",
										"name": "string"
									}
								}
							],
							"type": {
								"type": "reference",
								"typeArguments": [
									{
										"type": "union",
										"types": [
											{
												"type": "reference",
												"id": 26,
												"name": "User"
											},
											{
												"type": "intrinsic",
												"name": "null"
											}
										]
									}
								],
								"name": "Promise"
							}
						}
					],
					"sources": [
						{
							"fileName": "services/user/controller.ts",
							"line": 11,
							"character": 35
						}
					]
				}
			],
			"groups": [
				{
					"title": "Variables",
					"kind": 32,
					"children": [
						34
					]
				},
				{
					"title": "Functions",
					"kind": 64,
					"children": [
						47,
						52,
						36,
						44,
						41,
						38
					]
				}
			],
			"sources": [
				{
					"fileName": "services/user/controller.ts",
					"line": 1,
					"character": 0
				}
			]
		},
		{
			"id": 95,
			"name": "\"services/user/private/middlewares\"",
			"kind": 1,
			"kindString": "Module",
			"flags": {
				"isExported": true
			},
			"originalName": "/home/mrafael/PERSO/reftis-api/src/services/user/private/middlewares.ts",
			"children": [
				{
					"id": 100,
					"name": "deleteMe",
					"kind": 64,
					"kindString": "Function",
					"flags": {
						"isExported": true
					},
					"signatures": [
						{
							"id": 101,
							"name": "deleteMe",
							"kind": 4096,
							"kindString": "Call signature",
							"flags": {
								"isExported": true
							},
							"parameters": [
								{
									"id": 102,
									"name": "request",
									"kind": 32768,
									"kindString": "Parameter",
									"flags": {
										"isExported": true
									},
									"type": {
										"type": "reference",
										"name": "Request"
									}
								},
								{
									"id": 103,
									"name": "res",
									"kind": 32768,
									"kindString": "Parameter",
									"flags": {
										"isExported": true
									},
									"type": {
										"type": "reference",
										"name": "Response"
									}
								}
							],
							"type": {
								"type": "reference",
								"typeArguments": [
									{
										"type": "reference",
										"typeArguments": [
											{
												"type": "intrinsic",
												"name": "any"
											}
										],
										"name": "Response"
									}
								],
								"name": "Promise"
							}
						}
					],
					"sources": [
						{
							"fileName": "services/user/private/middlewares.ts",
							"line": 8,
							"character": 30
						}
					]
				},
				{
					"id": 96,
					"name": "updateMe",
					"kind": 64,
					"kindString": "Function",
					"flags": {
						"isExported": true
					},
					"signatures": [
						{
							"id": 97,
							"name": "updateMe",
							"kind": 4096,
							"kindString": "Call signature",
							"flags": {
								"isExported": true
							},
							"parameters": [
								{
									"id": 98,
									"name": "req",
									"kind": 32768,
									"kindString": "Parameter",
									"flags": {
										"isExported": true
									},
									"type": {
										"type": "reference",
										"name": "Request"
									}
								},
								{
									"id": 99,
									"name": "res",
									"kind": 32768,
									"kindString": "Parameter",
									"flags": {
										"isExported": true
									},
									"type": {
										"type": "reference",
										"name": "Response"
									}
								}
							],
							"type": {
								"type": "reference",
								"typeArguments": [
									{
										"type": "reference",
										"typeArguments": [
											{
												"type": "intrinsic",
												"name": "any"
											}
										],
										"name": "Response"
									}
								],
								"name": "Promise"
							}
						}
					],
					"sources": [
						{
							"fileName": "services/user/private/middlewares.ts",
							"line": 4,
							"character": 30
						}
					]
				}
			],
			"groups": [
				{
					"title": "Functions",
					"kind": 64,
					"children": [
						100,
						96
					]
				}
			],
			"sources": [
				{
					"fileName": "services/user/private/middlewares.ts",
					"line": 1,
					"character": 0
				}
			]
		},
		{
			"id": 104,
			"name": "\"services/user/private/routes\"",
			"kind": 1,
			"kindString": "Module",
			"flags": {
				"isExported": true
			},
			"originalName": "/home/mrafael/PERSO/reftis-api/src/services/user/private/routes.ts",
			"children": [
				{
					"id": 105,
					"name": "router",
					"kind": 32,
					"kindString": "Variable",
					"flags": {
						"isConst": true
					},
					"sources": [
						{
							"fileName": "services/user/private/routes.ts",
							"line": 6,
							"character": 12
						}
					],
					"type": {
						"type": "reference",
						"name": "Router"
					},
					"defaultValue": "Router()"
				}
			],
			"groups": [
				{
					"title": "Variables",
					"kind": 32,
					"children": [
						105
					]
				}
			],
			"sources": [
				{
					"fileName": "services/user/private/routes.ts",
					"line": 1,
					"character": 0
				}
			]
		},
		{
			"id": 60,
			"name": "\"services/user/public/middlewares\"",
			"kind": 1,
			"kindString": "Module",
			"flags": {
				"isExported": true
			},
			"originalName": "/home/mrafael/PERSO/reftis-api/src/services/user/public/middlewares.ts",
			"children": [
				{
					"id": 61,
					"name": "getUser",
					"kind": 64,
					"kindString": "Function",
					"flags": {
						"isExported": true
					},
					"signatures": [
						{
							"id": 62,
							"name": "getUser",
							"kind": 4096,
							"kindString": "Call signature",
							"flags": {
								"isExported": true
							},
							"parameters": [
								{
									"id": 63,
									"name": "req",
									"kind": 32768,
									"kindString": "Parameter",
									"flags": {
										"isExported": true
									},
									"type": {
										"type": "reference",
										"name": "Request"
									}
								},
								{
									"id": 64,
									"name": "res",
									"kind": 32768,
									"kindString": "Parameter",
									"flags": {
										"isExported": true
									},
									"type": {
										"type": "reference",
										"name": "Response"
									}
								}
							],
							"type": {
								"type": "reference",
								"typeArguments": [
									{
										"type": "reference",
										"typeArguments": [
											{
												"type": "intrinsic",
												"name": "any"
											}
										],
										"name": "Response"
									}
								],
								"name": "Promise"
							}
						}
					],
					"sources": [
						{
							"fileName": "services/user/public/middlewares.ts",
							"line": 10,
							"character": 29
						}
					]
				},
				{
					"id": 65,
					"name": "login",
					"kind": 64,
					"kindString": "Function",
					"flags": {
						"isExported": true
					},
					"signatures": [
						{
							"id": 66,
							"name": "login",
							"kind": 4096,
							"kindString": "Call signature",
							"flags": {
								"isExported": true
							},
							"parameters": [
								{
									"id": 67,
									"name": "req",
									"kind": 32768,
									"kindString": "Parameter",
									"flags": {
										"isExported": true
									},
									"type": {
										"type": "reference",
										"name": "Request"
									}
								},
								{
									"id": 68,
									"name": "res",
									"kind": 32768,
									"kindString": "Parameter",
									"flags": {
										"isExported": true
									},
									"type": {
										"type": "reference",
										"name": "Response"
									}
								}
							],
							"type": {
								"type": "reference",
								"typeArguments": [
									{
										"type": "reference",
										"typeArguments": [
											{
												"type": "intrinsic",
												"name": "any"
											}
										],
										"name": "Response"
									}
								],
								"name": "Promise"
							}
						}
					],
					"sources": [
						{
							"fileName": "services/user/public/middlewares.ts",
							"line": 37,
							"character": 27
						}
					]
				},
				{
					"id": 69,
					"name": "register",
					"kind": 64,
					"kindString": "Function",
					"flags": {
						"isExported": true
					},
					"signatures": [
						{
							"id": 70,
							"name": "register",
							"kind": 4096,
							"kindString": "Call signature",
							"flags": {
								"isExported": true
							},
							"parameters": [
								{
									"id": 71,
									"name": "req",
									"kind": 32768,
									"kindString": "Parameter",
									"flags": {
										"isExported": true
									},
									"type": {
										"type": "reference",
										"name": "Request"
									}
								},
								{
									"id": 72,
									"name": "res",
									"kind": 32768,
									"kindString": "Parameter",
									"flags": {
										"isExported": true
									},
									"type": {
										"type": "reference",
										"name": "Response"
									}
								}
							],
							"type": {
								"type": "reference",
								"typeArguments": [
									{
										"type": "reference",
										"typeArguments": [
											{
												"type": "intrinsic",
												"name": "any"
											}
										],
										"name": "Response"
									}
								],
								"name": "Promise"
							}
						}
					],
					"sources": [
						{
							"fileName": "services/user/public/middlewares.ts",
							"line": 57,
							"character": 30
						}
					]
				}
			],
			"groups": [
				{
					"title": "Functions",
					"kind": 64,
					"children": [
						61,
						65,
						69
					]
				}
			],
			"sources": [
				{
					"fileName": "services/user/public/middlewares.ts",
					"line": 1,
					"character": 0
				}
			]
		},
		{
			"id": 73,
			"name": "\"services/user/public/routes\"",
			"kind": 1,
			"kindString": "Module",
			"flags": {
				"isExported": true
			},
			"originalName": "/home/mrafael/PERSO/reftis-api/src/services/user/public/routes.ts",
			"children": [
				{
					"id": 74,
					"name": "router",
					"kind": 32,
					"kindString": "Variable",
					"flags": {
						"isExported": true,
						"isConst": true
					},
					"sources": [
						{
							"fileName": "services/user/public/routes.ts",
							"line": 5,
							"character": 12
						}
					],
					"type": {
						"type": "reference",
						"name": "Router"
					},
					"defaultValue": "Router()"
				}
			],
			"groups": [
				{
					"title": "Variables",
					"kind": 32,
					"children": [
						74
					]
				}
			],
			"sources": [
				{
					"fileName": "services/user/public/routes.ts",
					"line": 1,
					"character": 0
				}
			]
		},
		{
			"id": 106,
			"name": "\"services/user/routes\"",
			"kind": 1,
			"kindString": "Module",
			"flags": {
				"isExported": true
			},
			"originalName": "/home/mrafael/PERSO/reftis-api/src/services/user/routes.ts",
			"children": [
				{
					"id": 107,
					"name": "router",
					"kind": 32,
					"kindString": "Variable",
					"flags": {
						"isConst": true
					},
					"sources": [
						{
							"fileName": "services/user/routes.ts",
							"line": 6,
							"character": 12
						}
					],
					"type": {
						"type": "reference",
						"name": "Router"
					},
					"defaultValue": "Router()"
				}
			],
			"groups": [
				{
					"title": "Variables",
					"kind": 32,
					"children": [
						107
					]
				}
			],
			"sources": [
				{
					"fileName": "services/user/routes.ts",
					"line": 1,
					"character": 0
				}
			]
		},
		{
			"id": 10,
			"name": "\"types/index\"",
			"kind": 1,
			"kindString": "Module",
			"flags": {
				"isExported": true
			},
			"originalName": "/home/mrafael/PERSO/reftis-api/src/types/index.ts",
			"children": [
				{
					"id": 28,
					"name": "__global",
					"kind": 2,
					"kindString": "Namespace",
					"flags": {},
					"children": [
						{
							"id": 29,
							"name": "Express",
							"kind": 2,
							"kindString": "Namespace",
							"flags": {
								"isExported": true
							},
							"children": [
								{
									"id": 30,
									"name": "Request",
									"kind": 256,
									"kindString": "Interface",
									"flags": {
										"isExported": true
									},
									"children": [
										{
											"id": 31,
											"name": "token",
											"kind": 1024,
											"kindString": "Property",
											"flags": {
												"isExported": true
											},
											"sources": [
												{
													"fileName": "types/index.ts",
													"line": 8,
													"character": 17
												}
											],
											"type": {
												"type": "intrinsic",
												"name": "any"
											}
										},
										{
											"id": 32,
											"name": "user",
											"kind": 1024,
											"kindString": "Property",
											"flags": {
												"isExported": true
											},
											"sources": [
												{
													"fileName": "types/index.ts",
													"line": 9,
													"character": 16
												}
											],
											"type": {
												"type": "reference",
												"id": 26,
												"name": "User"
											}
										}
									],
									"groups": [
										{
											"title": "Properties",
											"kind": 1024,
											"children": [
												31,
												32
											]
										}
									],
									"sources": [
										{
											"fileName": "types/index.ts",
											"line": 7,
											"character": 32
										}
									]
								}
							],
							"groups": [
								{
									"title": "Interfaces",
									"kind": 256,
									"children": [
										30
									]
								}
							],
							"sources": [
								{
									"fileName": "types/index.ts",
									"line": 6,
									"character": 21
								}
							]
						}
					],
					"groups": [
						{
							"title": "Namespaces",
							"kind": 2,
							"children": [
								29
							]
						}
					],
					"sources": [
						{
							"fileName": "types/index.ts",
							"line": 5,
							"character": 14
						}
					]
				},
				{
					"id": 11,
					"name": "Code",
					"kind": 4,
					"kindString": "Enumeration",
					"flags": {
						"isExported": true
					},
					"children": [
						{
							"id": 14,
							"name": "ACCEPTED",
							"kind": 16,
							"kindString": "Enumeration member",
							"flags": {
								"isExported": true
							},
							"sources": [
								{
									"fileName": "types/index.ts",
									"line": 17,
									"character": 12
								}
							]
						},
						{
							"id": 16,
							"name": "BAD_REQUEST",
							"kind": 16,
							"kindString": "Enumeration member",
							"flags": {
								"isExported": true
							},
							"sources": [
								{
									"fileName": "types/index.ts",
									"line": 20,
									"character": 15
								}
							],
							"defaultValue": "400"
						},
						{
							"id": 22,
							"name": "CONFLICT",
							"kind": 16,
							"kindString": "Enumeration member",
							"flags": {
								"isExported": true
							},
							"sources": [
								{
									"fileName": "types/index.ts",
									"line": 26,
									"character": 12
								}
							],
							"defaultValue": "409"
						},
						{
							"id": 13,
							"name": "CREATED",
							"kind": 16,
							"kindString": "Enumeration member",
							"flags": {
								"isExported": true
							},
							"sources": [
								{
									"fileName": "types/index.ts",
									"line": 16,
									"character": 11
								}
							]
						},
						{
							"id": 18,
							"name": "FORBIDDEN",
							"kind": 16,
							"kindString": "Enumeration member",
							"flags": {
								"isExported": true
							},
							"sources": [
								{
									"fileName": "types/index.ts",
									"line": 22,
									"character": 13
								}
							],
							"defaultValue": "403"
						},
						{
							"id": 23,
							"name": "INTERNAL_SERVER_ERROR",
							"kind": 16,
							"kindString": "Enumeration member",
							"flags": {
								"isExported": true
							},
							"sources": [
								{
									"fileName": "types/index.ts",
									"line": 28,
									"character": 25
								}
							],
							"defaultValue": "500"
						},
						{
							"id": 20,
							"name": "METHOD_NOT_ALLOWED",
							"kind": 16,
							"kindString": "Enumeration member",
							"flags": {
								"isExported": true
							},
							"sources": [
								{
									"fileName": "types/index.ts",
									"line": 24,
									"character": 22
								}
							]
						},
						{
							"id": 21,
							"name": "NOT_ACCEPTABLE",
							"kind": 16,
							"kindString": "Enumeration member",
							"flags": {
								"isExported": true
							},
							"sources": [
								{
									"fileName": "types/index.ts",
									"line": 25,
									"character": 18
								}
							]
						},
						{
							"id": 19,
							"name": "NOT_FOUND",
							"kind": 16,
							"kindString": "Enumeration member",
							"flags": {
								"isExported": true
							},
							"sources": [
								{
									"fileName": "types/index.ts",
									"line": 23,
									"character": 13
								}
							]
						},
						{
							"id": 24,
							"name": "NOT_IMPLEMENTED",
							"kind": 16,
							"kindString": "Enumeration member",
							"flags": {
								"isExported": true
							},
							"sources": [
								{
									"fileName": "types/index.ts",
									"line": 29,
									"character": 19
								}
							]
						},
						{
							"id": 15,
							"name": "NO_CONTENT",
							"kind": 16,
							"kindString": "Enumeration member",
							"flags": {
								"isExported": true
							},
							"sources": [
								{
									"fileName": "types/index.ts",
									"line": 18,
									"character": 14
								}
							],
							"defaultValue": "204"
						},
						{
							"id": 12,
							"name": "OK",
							"kind": 16,
							"kindString": "Enumeration member",
							"flags": {
								"isExported": true
							},
							"sources": [
								{
									"fileName": "types/index.ts",
									"line": 15,
									"character": 6
								}
							],
							"defaultValue": "200"
						},
						{
							"id": 25,
							"name": "SERVICE_UNAVAILABLE",
							"kind": 16,
							"kindString": "Enumeration member",
							"flags": {
								"isExported": true
							},
							"sources": [
								{
									"fileName": "types/index.ts",
									"line": 30,
									"character": 23
								}
							],
							"defaultValue": "503"
						},
						{
							"id": 17,
							"name": "UNAUTHORIZED",
							"kind": 16,
							"kindString": "Enumeration member",
							"flags": {
								"isExported": true
							},
							"sources": [
								{
									"fileName": "types/index.ts",
									"line": 21,
									"character": 16
								}
							]
						}
					],
					"groups": [
						{
							"title": "Enumeration members",
							"kind": 16,
							"children": [
								14,
								16,
								22,
								13,
								18,
								23,
								20,
								21,
								19,
								24,
								15,
								12,
								25,
								17
							]
						}
					],
					"sources": [
						{
							"fileName": "types/index.ts",
							"line": 14,
							"character": 16
						}
					]
				},
				{
					"id": 26,
					"name": "User",
					"kind": 4194304,
					"kindString": "Type alias",
					"flags": {
						"isExported": true
					},
					"sources": [
						{
							"fileName": "types/index.ts",
							"line": 3,
							"character": 16
						}
					],
					"type": {
						"type": "reference",
						"typeArguments": [
							{
								"type": "reflection",
								"declaration": {
									"id": 27,
									"name": "__type",
									"kind": 65536,
									"kindString": "Type literal",
									"flags": {
										"isExported": true
									},
									"sources": [
										{
											"fileName": "types/index.ts",
											"line": 3,
											"character": 34
										}
									]
								}
							}
						],
						"name": "UserGetPayload"
					}
				}
			],
			"groups": [
				{
					"title": "Namespaces",
					"kind": 2,
					"children": [
						28
					]
				},
				{
					"title": "Enumerations",
					"kind": 4,
					"children": [
						11
					]
				},
				{
					"title": "Type aliases",
					"kind": 4194304,
					"children": [
						26
					]
				}
			],
			"sources": [
				{
					"fileName": "types/index.ts",
					"line": 1,
					"character": 0
				}
			]
		},
		{
			"id": 1,
			"name": "\"util/logger\"",
			"kind": 1,
			"kindString": "Module",
			"flags": {
				"isExported": true
			},
			"originalName": "/home/mrafael/PERSO/reftis-api/src/util/logger.ts",
			"children": [
				{
					"id": 2,
					"name": "consoleFormat",
					"kind": 32,
					"kindString": "Variable",
					"flags": {
						"isConst": true
					},
					"sources": [
						{
							"fileName": "util/logger.ts",
							"line": 3,
							"character": 19
						}
					],
					"type": {
						"type": "reference",
						"name": "Format"
					},
					"defaultValue": "winston.format.combine(\n    winston.format.colorize(),\n    winston.format.timestamp({format: \"YYYY-MM-DD HH:mm:ss\"}),\n    winston.format.align(),\n    winston.format.printf(\n        info => `${info.timestamp} ${info.level}: ${info.message}`,\n    )\n)"
				},
				{
					"id": 3,
					"name": "fileFormat",
					"kind": 32,
					"kindString": "Variable",
					"flags": {
						"isConst": true
					},
					"sources": [
						{
							"fileName": "util/logger.ts",
							"line": 12,
							"character": 16
						}
					],
					"type": {
						"type": "reference",
						"name": "Format"
					},
					"defaultValue": "winston.format.combine(\n    winston.format.timestamp({format: \"YYYY-MM-DD HH:mm:ss\"}),\n    winston.format.align(),\n    winston.format.printf(\n        info => `${info.timestamp} ${info.level}: ${info.message}`,\n    )\n)"
				},
				{
					"id": 7,
					"name": "logger",
					"kind": 32,
					"kindString": "Variable",
					"flags": {
						"isExported": true,
						"isConst": true
					},
					"sources": [
						{
							"fileName": "util/logger.ts",
							"line": 50,
							"character": 12
						}
					],
					"type": {
						"type": "reference",
						"name": "Logger"
					},
					"defaultValue": "winston.createLogger(options)"
				},
				{
					"id": 4,
					"name": "testFormat",
					"kind": 32,
					"kindString": "Variable",
					"flags": {
						"isConst": true
					},
					"sources": [
						{
							"fileName": "util/logger.ts",
							"line": 20,
							"character": 16
						}
					],
					"type": {
						"type": "reference",
						"name": "Format"
					},
					"defaultValue": "winston.format.combine(\n    // winston.format.label({ label: path.basename(process.mainModule.filename) }),\n    winston.format.timestamp({format: \"YYYY-MM-DD HH:mm:ss\"}),\n    // Format the metadata object\n    winston.format.metadata({fillExcept: [\"message\", \"level\", \"timestamp\", \"label\"]}),\n    winston.format.json(),\n    winston.format.prettyPrint()\n)"
				},
				{
					"id": 5,
					"name": "options",
					"kind": 2097152,
					"kindString": "Object literal",
					"flags": {
						"isConst": true
					},
					"children": [
						{
							"id": 6,
							"name": "transports",
							"kind": 32,
							"kindString": "Variable",
							"flags": {},
							"sources": [
								{
									"fileName": "util/logger.ts",
									"line": 30,
									"character": 14
								}
							],
							"type": {
								"type": "array",
								"elementType": {
									"type": "union",
									"types": [
										{
											"type": "reference",
											"name": "ConsoleTransportInstance"
										},
										{
											"type": "reference",
											"name": "FileTransportInstance"
										}
									]
								}
							},
							"defaultValue": "[\n        new winston.transports.Console({\n            level: process.env.NODE_ENV === \"production\" ? \"error\" : \"debug\",\n            format: consoleFormat\n        }),\n        new winston.transports.File({\n            dirname: \"log\",\n            filename: \"debug.log\",\n            level: \"debug\",\n            format: fileFormat\n        }),\n        new winston.transports.File({\n            dirname: \"log\",\n            filename: \"error.log\",\n            level: \"error\",\n            format: testFormat\n        })\n    ]"
						}
					],
					"groups": [
						{
							"title": "Variables",
							"kind": 32,
							"children": [
								6
							]
						}
					],
					"sources": [
						{
							"fileName": "util/logger.ts",
							"line": 29,
							"character": 13
						}
					],
					"type": {
						"type": "intrinsic",
						"name": "object"
					}
				}
			],
			"groups": [
				{
					"title": "Variables",
					"kind": 32,
					"children": [
						2,
						3,
						7,
						4
					]
				},
				{
					"title": "Object literals",
					"kind": 2097152,
					"children": [
						5
					]
				}
			],
			"sources": [
				{
					"fileName": "util/logger.ts",
					"line": 1,
					"character": 0
				}
			]
		},
		{
			"id": 8,
			"name": "\"util/secrets\"",
			"kind": 1,
			"kindString": "Module",
			"flags": {
				"isExported": true
			},
			"originalName": "/home/mrafael/PERSO/reftis-api/src/util/secrets.ts",
			"children": [
				{
					"id": 9,
					"name": "ENVIRONMENT",
					"kind": 32,
					"kindString": "Variable",
					"flags": {
						"isExported": true,
						"isConst": true
					},
					"sources": [
						{
							"fileName": "util/secrets.ts",
							"line": 10,
							"character": 24
						}
					],
					"type": {
						"type": "union",
						"types": [
							{
								"type": "intrinsic",
								"name": "undefined"
							},
							{
								"type": "intrinsic",
								"name": "string"
							}
						]
					},
					"defaultValue": "process.env.NODE_ENV"
				}
			],
			"groups": [
				{
					"title": "Variables",
					"kind": 32,
					"children": [
						9
					]
				}
			],
			"sources": [
				{
					"fileName": "util/secrets.ts",
					"line": 1,
					"character": 0
				}
			]
		}
	],
	"groups": [
		{
			"title": "Modules",
			"kind": 1,
			"children": [
				110,
				112,
				113,
				55,
				75,
				108,
				33,
				95,
				104,
				60,
				73,
				106,
				10,
				1,
				8
			]
		}
	]
}