import {PrismaClient} from "@prisma/client";

const prisma = new PrismaClient();

export async function create(name: string, userID: number){
    return await prisma.magazine.create({
        data: {
            name: name,
            User: {
                connect: {id: userID}
            }
        }
    });
}

export async function addNews(magazineID: number, userID: number, newsID: number){
    const mag = await prisma.magazine.findMany({
        where: {
            User: { id: userID },
            id: Number(magazineID),
        }
    });

    if (mag.length > 0) {
        return await prisma.news.update({
            where: {
                id: Number(newsID),
            },
            data: {
                Magazine: {
                    connect: {id: Number(magazineID)}
                }
            }
        });
    } else {
        return [];
    }
}

export async function removeNews(magazineID: number, userID: number, newsID: number){
    const mag = await prisma.magazine.findMany({
        where: {
            User: { id: userID },
            id: Number(magazineID),
        }
    });

    if (mag.length > 0) {
        return await prisma.news.update({
            where: {
                id: Number(newsID),
            },
            data: {
                Magazine: {
                    disconnect: true
                }
            }
        });
    } else {
        return [];
    }
}


export async function rename(id: number, userID: number, newName: string){
    const mag = await prisma.magazine.findMany({
        where: {
            User: { id: userID },
            id: Number(id),
        }
    });

    if (mag.length > 0) {
        return await prisma.magazine.update({
            where: {
                id: Number(id)
            },
            data: {
                name: newName
            }
        });
    } else {
        return [];
    }
}

export async function remove(id: number, userID: number, ){
    const mag = await prisma.magazine.findMany({
        where: {
            User: { id: userID },
            id: Number(id),
        }
    });

    if (mag.length > 0) {
        return await prisma.magazine.delete({
            where: {
                id: Number(id)
            }
        });
    } else {
        return [];
    }
}

export async function getAll(userID: number){
    return await prisma.magazine.findMany({
        where: {
            User: {
                id: userID
            }
        },
        include: {
            News: {}
        }
    });
}