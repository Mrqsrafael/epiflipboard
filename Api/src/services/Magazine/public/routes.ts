import {Router} from "express";

import {
    addNewsToMagazine,
    createMagazine,
    deleteMagazine,
    getMagazine,
    removeNewsToMagazine,
    updateMagazine
} from "./middlewares";

import {userAuthMiddlewares} from "../../auth/middlewares";

const router = Router();

router.post("", [...userAuthMiddlewares, createMagazine])
router.put("", [...userAuthMiddlewares, updateMagazine])
router.put("/addNews", [...userAuthMiddlewares, addNewsToMagazine])
router.put("/removeNews", [...userAuthMiddlewares, removeNewsToMagazine])
router.delete("", [...userAuthMiddlewares, deleteMagazine])
router.get("", [...userAuthMiddlewares, getMagazine])

export default router;