import {Request, response, Response} from "express";
import {Code} from "../../../types";

import * as MagController from "../controller";
import {ManageErrorMessage} from "../../../error/ErrorHandling";


/**
 * This function create a magazine with the specified name
 * @params :
 *      name (string)
 * @returns 200 if OK
 */


export async function createMagazine(req: Request, res: Response) {
    const {user} = req;

    const { name } = req.body;

    if (!user) {
        const error = ManageErrorMessage("global",
            "required",
            "login required",
            "header",
            "user",
            Code.BAD_REQUEST,
            "The user was undefined");

        return res.status(Code.BAD_REQUEST).json({error});
    } else if  (!name) {
        const error = ManageErrorMessage("global",
            "required",
            "name required",
            "parameter",
            "name",
            Code.BAD_REQUEST,
            "The name was undefined" );

        return res.status(Code.BAD_REQUEST).json( {error});
    }

    try {
        const myMag = await MagController.create(name, user.id);
        return res.status(Code.OK).json({myMag});
    } catch {
        const error = ManageErrorMessage("global",
            "server",
            "internal server error",
            "server",
            "createMagazine",
            Code.INTERNAL_SERVER_ERROR,
            "undefined" );

        return res.status(Code.INTERNAL_SERVER_ERROR).json({error});}
}

/**
 * This function add a news to the  specified user magazine
 * @params :
 *      magazineID
 *      newsID
 * @returns 200 if OK
 */

export async function addNewsToMagazine(req: Request, res: Response) {
    const {user} = req;

    const { magazineID, newsID } = req.body;

    if (!magazineID || !newsID) {
        const error = ManageErrorMessage("global",
            "required",
            "magazineID and newsID required",
            "parameter",
            "magazineID / newsID",
            Code.BAD_REQUEST,
            "The newsID or the magazine ID was undefined");

        return res.status(Code.BAD_REQUEST).json( {error});
    }
    try {
        const myMag = await MagController.addNews(magazineID, user.id, newsID);

        if (!Array.isArray(myMag) || myMag.length < 1)   {

            const error = ManageErrorMessage("global",
                "Invalid argument",
                "newsID / MagazineID invalid",
                "parameter",
                "magazineID / newsID",
                Code.BAD_REQUEST,
                "The newsID or the magazine ID was not relate to a Mag / news");

            return res.status(Code.BAD_REQUEST).json( {error});
        }

        return res.status(Code.OK).json({myMag});
    } catch {
        return res.status(Code.BAD_REQUEST);
    }

}

/**
 * This function remove the specified news of the specified magazine
 *  @params :
 *      magazineID
 *      newsID
 * @returns 200 if OK
 */

export async function removeNewsToMagazine(req: Request, res: Response) {
    const {user} = req;

    const { magazineID, newsID } = req.body;

    if (!magazineID || !newsID || !user) {
        const error = ManageErrorMessage("global",
            "required",
            "magazineID and newsID required",
            "parameter",
            "magazineID / newsID",
            Code.BAD_REQUEST,
            "The newsID or the magazineID was undefined");

        return res.status(Code.BAD_REQUEST).json( {error});
    }

    try {
        const myMag = await MagController.removeNews(magazineID, user.id, newsID);

        if (!Array.isArray(myMag) || myMag.length < 1)   {
            const error = ManageErrorMessage("global",
                "Invalid argument",
                "newsID / MagazineID invalid",
                "parameter",
                "magazineID / newsID",
                Code.BAD_REQUEST,
                "The newsID or the magazine ID was not relate to a Mag / news");

            return res.status(Code.BAD_REQUEST).json( {error});
        }
        return res.status(Code.OK).json({myMag});
    } catch {
        return res.status(Code.BAD_REQUEST);
    }

}

/**
 * This function rename the magazine
 * @params :
 *      name(string)
 *      id(magazineID)
 * @returns 200 if OK
 */

export async function updateMagazine(req: Request, res: Response) {
    const {user} = req;

    const { name, id } = req.body;

    if (!name || !id || !user) {
        const error = ManageErrorMessage("global",
            "required",
            "name and id required",
            "parameter",
            "name / id",
            Code.BAD_REQUEST,
            "The name or the id was undefined");

        return res.status(Code.BAD_REQUEST).json( {error});
    }

    try {
        const myMag = await MagController.rename(id, user.id, name);

        if (!Array.isArray(myMag) || myMag.length < 1)   {
            const error = ManageErrorMessage("global",
                "Invalid argument",
                "id invalid",
                "parameter",
                "id",
                Code.BAD_REQUEST,
                "The id was not relate to a Mag");

            return res.status(Code.BAD_REQUEST).json( {error});
        }

        return res.status(Code.OK).json({myMag});
    } catch {
        return res.status(Code.BAD_REQUEST);
    }
}

/**
 * This function delete the specified magazine
 * @params :
 *      id(MagazineID)
 * @returns 200 if OK
 */

export async function deleteMagazine(req: Request, res: Response) {
    const {user} = req;

    const { id } = req.body;

    if (!id || !user) {
        const error = ManageErrorMessage("global",
            "required",
            "id required",
            "parameter",
            "id",
            Code.BAD_REQUEST,
            "The id was undefined");

        return res.status(Code.BAD_REQUEST).json( {error});
    }

    try {
        const myMag = await MagController.remove(id, user.id);

        if (!Array.isArray(myMag) || myMag.length < 1) {
            const error = ManageErrorMessage("global",
                "Invalid argument",
                "id invalid",
                "parameter",
                "id",
                Code.BAD_REQUEST,
                "The id was not relate to a Mag");

            return res.status(Code.BAD_REQUEST).json( {error});
        }

        return res.status(Code.OK).json({myMag});
    } catch {
        return res.status(Code.BAD_REQUEST);
    }
}

/**
 * This function get every magazine of the specified user
 * @returns 200 if OK
 */

export async function getMagazine(req: Request, res: Response) {
    const {user} = req;

    if (user) {
        try {
            const myMag = await MagController.getAll(user.id);
            return res.status(Code.OK).json({myMag});
        } catch {
            return res.status(Code.BAD_REQUEST);
        }
    } else {
        res.statusMessage = "The user was undefined";
        return res.status(Code.BAD_REQUEST);
    }
}