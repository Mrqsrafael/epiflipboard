import {Router} from "express";

import {favoriteSource, getAllFavoriteSources, getfavoriteSources} from "./middlewares";
import {userAuthMiddlewares} from "../../auth/middlewares";
import {getAll} from "../../user/controller";

const router = Router();

router.post("", [...userAuthMiddlewares, favoriteSource]);
router.get("", [...userAuthMiddlewares, getfavoriteSources]);
router.get("/all", [getAllFavoriteSources]);



// @ts-ignore
export default router;