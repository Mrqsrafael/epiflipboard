import {Router} from "express";

import {userAuthMiddlewares} from "../../auth/middlewares";
import {deleteMe, updateMe} from "./middlewares";

const router = Router();

router.put("/users", [...userAuthMiddlewares, updateMe]);
router.post("/me/delete", [...userAuthMiddlewares, deleteMe]);

export default router;
