import {Request, Response} from "express";
import {Code} from "../../../types";

export async function updateMe(req: Request, res: Response) {
    return res.sendStatus(Code.NOT_IMPLEMENTED);
}

export async function deleteMe(request: Request, res: Response) {
    return res.sendStatus(Code.NOT_IMPLEMENTED);
}

