import {Request, response, Response} from "express";
import axios from "axios";

import { Code } from "../../../types";
import * as NewsController from "../controller";
import { title } from "process";

/**
* This function look for the last news on the Google News API
 * @returns 200 if OK
*/

export async function getNews(req: Request, res: Response) {
    const {country} = req.body;

    var url = 'http://newsapi.org/v2/top-headlines?' +
        'country=' + country + '&' +
        'apiKey=5cdeb32c1ad24c818f1c6588dbd8601c';

    const {data} = await axios.get(url);

    return res.status(Code.OK).json({data});
}

export async function getNewsFrom(req: Request, res: Response) {
    const {source} = req.body;

    var url = 'http://newsapi.org/v2/top-headlines?' +
        'sources=' + source + '&' +
        'apiKey=5cdeb32c1ad24c818f1c6588dbd8601c';

    const {data} = await axios.get(url);

    return res.status(Code.OK).json({data});
}

export async function getTopHeadlinesFromCategory(req: Request, res: Response) {
    const {category} = req.body;

    var url = 'http://newsapi.org/v2/top-headlines?' +
        'category=' + category + '&' +
        'apiKey=5cdeb32c1ad24c818f1c6588dbd8601c';

    const {data} = await axios.get(url);

    return res.status(Code.OK).json({data});
}


/**
 * This function look for every news from the subject in the specified language
 * @params:
 *      search(string)
 *      language(string, ex : fr)
 * @returns 200 if OK
 */


export async function search(req: Request, res: Response) {
    const {search, language} = req.body;

    const url = "http://newsapi.org/v2/everything?" +
        "q=" + search + "&language=" + language +
        "&apiKey=5cdeb32c1ad24c818f1c6588dbd8601c";

    const {data} = await axios.get(url);

    return res.status(Code.OK).json({data});
}

export async function suggestion(req: Request, res: Response) {
    const {user} = req;

    let data;

    if (user) {
        data = await NewsController.newsForYou(user.id);
    }
    return res.status(Code.OK).json({data});
}

export async function getnews(req: Request, res: Response) {
    const { nbritem, page, category } = req.body;

    const news = await NewsController.getnewsAPI(Number(nbritem) * (Number(page) - 1), Number(nbritem), category);

    return res.status(Code.OK).json({news})
}

export async function testnews(req: Request, res: Response) {
    // if more that 1 day then make request
    if (await NewsController.checkBeforeFeed() == true) {
        res.statusMessage = "Update data, 0 doublon found";
        return res;
    }
    const url = "https://newsapi.org/v2/top-headlines?country=fr&apiKey=5cdeb32c1ad24c818f1c6588dbd8601c";

    const { data } = await axios.get(url);
    // add news to database
    for( const newsDetails in data.articles ) {
        NewsController.createNews(data.articles[newsDetails]["title"],
            data.articles[newsDetails]["url"],
            data.articles[newsDetails]["urlToImage"],
            data.articles[newsDetails]["description"],
            data.articles[newsDetails]["source"]["name"],
            "France");
    }
    
    const url1 = "https://newsapi.org/v2/top-headlines?country=us&apiKey=5cdeb32c1ad24c818f1c6588dbd8601c";

    const test = await axios.get(url1);
    for( const newsDetails in test.data.articles ) {
        console.log(test.data.articles[newsDetails]["title"])
        NewsController.createNews(test.data.articles[newsDetails]["title"],
        test.data.articles[newsDetails]["url"],
        test.data.articles[newsDetails]["urlToImage"],
        test.data.articles[newsDetails]["description"],
            test.data.articles[newsDetails]["source"]["name"],
        "us");
    }

    let url2 = "https://newsapi.org/v2/top-headlines?country=fr&category=business&apiKey=5cdeb32c1ad24c818f1c6588dbd8601c";

    const test1 = await axios.get(url2);
    for( const newsDetails in test1.data.articles ) {
        console.log(test1.data.articles[newsDetails]["title"])
        NewsController.createNews(test1.data.articles[newsDetails]["title"],
        test1.data.articles[newsDetails]["url"],
        test1.data.articles[newsDetails]["urlToImage"],
        test1.data.articles[newsDetails]["description"],
            test1.data.articles[newsDetails]["source"]["name"],
        "business");
    }

    let url3 = "https://newsapi.org/v2/top-headlines?country=us&category=business&apiKey=5cdeb32c1ad24c818f1c6588dbd8601c";

    const test2 = await axios.get(url3);
    for( const newsDetails in test2.data.articles ) {
        console.log(test2.data.articles[newsDetails]["title"])
        NewsController.createNews(test2.data.articles[newsDetails]["title"],
        test2.data.articles[newsDetails]["url"],
        test2.data.articles[newsDetails]["urlToImage"],
        test2.data.articles[newsDetails]["description"],
            test2.data.articles[newsDetails]["source"]["name"],
        "business");
    }

    url2 = "https://newsapi.org/v2/top-headlines?country=fr&category=sport&apiKey=5cdeb32c1ad24c818f1c6588dbd8601c";

    const test3 = await axios.get(url2);
    for( const newsDetails in test3.data.articles ) {
        console.log(test3.data.articles[newsDetails]["title"])
        NewsController.createNews(test3.data.articles[newsDetails]["title"],
        test3.data.articles[newsDetails]["url"],
        test3.data.articles[newsDetails]["urlToImage"],
        test3.data.articles[newsDetails]["description"],
            test3.data.articles[newsDetails]["source"]["name"],
        "sport");
    }

    url3 = "https://newsapi.org/v2/top-headlines?country=us&category=sport&apiKey=5cdeb32c1ad24c818f1c6588dbd8601c";

    const test4 = await axios.get(url3);
    for( var newsdetails in test4.data.articles ) {
        console.log(test4.data.articles[newsdetails]["title"])
        NewsController.createNews(test4.data.articles[newsdetails]["title"],
        test4.data.articles[newsdetails]["url"],
        test4.data.articles[newsdetails]["urlToImage"],
        test4.data.articles[newsdetails]["description"],
            test4.data.articles[newsdetails]["source"]["name"],
        "sport");
    }

    const url4 = "https://newsapi.org/v2/top-headlines?country=fr&category=science&apiKey=5cdeb32c1ad24c818f1c6588dbd8601c";

    const test5 = await axios.get(url4);
    for( var newsdetails in test5.data.articles ) {
        console.log(test5.data.articles[newsdetails]["title"])
        NewsController.createNews(test5.data.articles[newsdetails]["title"],
        test5.data.articles[newsdetails]["url"],
        test5.data.articles[newsdetails]["urlToImage"],
        test5.data.articles[newsdetails]["description"],
            test5.data.articles[newsdetails]["source"]["name"],
        "science");
    }

    const url5 = "https://newsapi.org/v2/top-headlines?country=us&category=science&apiKey=5cdeb32c1ad24c818f1c6588dbd8601c";

    const test6 = await axios.get(url5);
    for( var newsdetails in test6.data.articles ) {
        console.log(test6.data.articles[newsdetails]["title"])
        NewsController.createNews(test6.data.articles[newsdetails]["title"],
        test6.data.articles[newsdetails]["url"],
        test6.data.articles[newsdetails]["urlToImage"],
        test6.data.articles[newsdetails]["description"],
            test6.data.articles[newsdetails]["source"]["name"],
        "science");
    }
    const url6 = "https://newsapi.org/v2/top-headlines?country=fr&category=health&apiKey=5cdeb32c1ad24c818f1c6588dbd8601c";

    const test7 = await axios.get(url6);
    for( const newsDetails in test7.data.articles ) {
        console.log(test7.data.articles[newsDetails]["title"])
        NewsController.createNews(test7.data.articles[newsDetails]["title"],
        test7.data.articles[newsDetails]["url"],
        test7.data.articles[newsDetails]["urlToImage"],
        test7.data.articles[newsDetails]["description"],
            test7.data.articles[newsDetails]["source"]["name"],
        "health");
    }

    const url7 = "https://newsapi.org/v2/top-headlines?country=us&category=health&apiKey=5cdeb32c1ad24c818f1c6588dbd8601c";

    const test8 = await axios.get(url5);
    for( const newsDetails in test8.data.articles ) {
        console.log(test8.data.articles[newsDetails]["title"])
        NewsController.createNews(test8.data.articles[newsDetails]["title"],
        test8.data.articles[newsDetails]["url"],
        test8.data.articles[newsDetails]["urlToImage"],
        test8.data.articles[newsDetails]["description"],
            test8.data.articles[newsDetails]["source"]["name"],
        "health");
    }
    const url8 = "https://newsapi.org/v2/top-headlines?country=fr&category=technology&apiKey=5cdeb32c1ad24c818f1c6588dbd8601c";

    const test9 = await axios.get(url8);
    for( const newsDetails in test9.data.articles ) {
        console.log(test9.data.articles[newsDetails]["title"])
        NewsController.createNews(test9.data.articles[newsDetails]["title"],
        test9.data.articles[newsDetails]["url"],
        test9.data.articles[newsDetails]["urlToImage"],
        test9.data.articles[newsDetails]["description"],
            test9.data.articles[newsDetails]["source"]["name"],
        "technology");
    }

    var url9 = "https://newsapi.org/v2/top-headlines?country=us&category=technology&apiKey=5cdeb32c1ad24c818f1c6588dbd8601c";

    const test10 = await axios.get(url5);
    for( const newsDetails in test10.data.articles ) {
        console.log(test10.data.articles[newsDetails]["title"])
        NewsController.createNews(test10.data.articles[newsDetails]["title"],
        test10.data.articles[newsDetails]["url"],
        test10.data.articles[newsDetails]["urlToImage"],
        test10.data.articles[newsDetails]["description"],
            test10.data.articles[newsDetails]["source"]["name"],
        "technology");
    }
    res.statusMessage = "Update data, 0 doublon found";
    return res.status(Code.OK).json({});
}