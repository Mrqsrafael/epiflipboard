import {Router} from "express";

import {getNews, getTopHeadlinesFromCategory, getNewsFrom, search, testnews, suggestion, getnews} from "./middlewares";
import {userAuthMiddlewares} from "../../auth/middlewares";

const router = Router();

router.get("/news", [getNews]);
router.get("/newsFrom", [getNewsFrom]);
router.get("/newsCategory", [getTopHeadlinesFromCategory]);
router.get("/search", [search]);
router.get("/test", [testnews]);
router.get("/getnews", [getnews]);
router.get("/suggestion", [...userAuthMiddlewares, suggestion]);


// @ts-ignore
export default router;