import {PrismaClient} from "@prisma/client";
import {getUserFavoriteCategories} from "../favCategories/controller";
import {getUserFavoriteSources} from "../favSource/controller";
import axios from "axios";

const prisma = new PrismaClient();

export async function create(title: string, url: string, urlimage: string, description: string, sourceName: string, category: string) {
    return await prisma.news.create({
        data: {
            title,
            url,
            urlimage,
            description,
            sourceName,
            category
        }
    });
}

export async function getnewsAPI(skip: number, nbr: number, category: string) {
    console.log(category);
    if (category == "") {
        const news = await prisma.news.findMany({
/*
            first: nbr,
*/
            skip: skip
        });
        return news;
    } else {
        const news1 = await prisma.news.findMany({
/*
            first: Number(nbr),
*/
            skip: Number(skip),
            where: {category: category}
        });
        return news1;
    }
}

export async function newsForYou(id: number) {
    const favoriteCategories = await getUserFavoriteCategories(id);
    const favoriteSources = await getUserFavoriteSources(id);

    let dailyNews = [];

    for (const value of favoriteCategories) {
        let url = "https://newsapi.org/v2/top-headlines?country=" + "fr"  + "&pageSize=3&category=";

        url += value.name;
        url += "&apiKey=5cdeb32c1ad24c818f1c6588dbd8601c";

        const { data } = await axios.get(url);

        dailyNews.push(data);

    };

    for (const value of favoriteSources) {
        let url = "https://newsapi.org/v2/top-headlines?pageSize=3&sources=";

        url += value.name;
        url += "&apiKey=5cdeb32c1ad24c818f1c6588dbd8601c";

        const { data } = await axios.get(url);

        dailyNews.push(data);

    }

    return dailyNews;
}

function formatDate(date: Date) {
/*    var d = new Date(date),
        month = '' + (d.getMonth() + 1),
        day = '' + d.getDate(),
        year = d.getFullYear();

    if (month.length < 2)
        month = '0' + month;
    if (day.length < 2)
        day = '0' + day;

    return [year, month, day].join('-');*/
}

export async function checkBeforeFeed() {
/*    const news = await prisma.news.findMany({last: 1})
    if (formatDate(news[0]["createdAt"]) == formatDate(new Date())) {
        return true;
    } */
    return false;
}

export async function createNews(title: string, url: string, urlimage: string, description: string, sourceName: string, category: string) {
    const newsclone = await prisma.news.findMany({
        where: {url: url},
    });
    const newscloneNbr = newsclone.length;

    if (newscloneNbr == 0) {
        return await prisma.news.create({
            data: {
                title,
                url,
                urlimage,
                description,
                sourceName,
                category
            }
        });
    } else {
        const newsclonedelete = await prisma.news.deleteMany({
            where: {url: url},
        });
        return await prisma.news.create({
            data: {
                title,
                url,
                urlimage,
                description,
                sourceName,
                category
            }
        });
    }
}
