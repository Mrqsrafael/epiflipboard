import {NextFunction, Request, Response} from "express";
import {Bearer} from "permit";
import * as jwt from "jsonwebtoken";

import {Code, User} from "../../types";
import {getByID} from "../user/controller";
import logger from "../../util/logger";

const jwtKey = process.env["JWT_KEY"];
const permit = new Bearer({
    query: "access_token", // Also allow an `?access_token=` query parameter.
});

/**
 *
 * @param req
 * @param res
 * @param next
 */

export function extractToken(req: Request, res: Response, next: NextFunction) {
    if (!jwtKey)
        return new Error("No key");
    // Try to find the bearer token in the request.
    const token = permit.check(req);

    // No token found, so ask for authentication.
    if (!token) {
        logger.debug("extractToken: Token is null");
        permit.fail(res);
        return next(new Error("Authentication required!"));
    }
    req.token = token;
    return next();
}

/**
 *
 * @param req
 * @param res
 * @param next
 */

export async function authenticateToken(req: Request, res: Response, next: NextFunction) {
    const payload = jwt.decode(req.token);

    if (!payload) {
        logger.debug("Authenticate: Payload is null");
        permit.fail(res);
        return next(new Error("Authentication required!"));
    }

    // @ts-ignore
    const id = payload?.id;

    const user: User | null = await getByID(id);
    if (!user) {
        permit.fail(res);
        return next(new Error("Authentication invalid!"));
    }
    req.user = user;
    return next();

    // Perform your authentication logic however you'd like...
    // db.users.findByToken(token, (err: Error, user) => {
    //     if (err) return next(err)
    //
    //     // No user found, so their token was invalid.
    //     if (!user) {
    //         permit.fail(res)
    //         return next(new Error(`Authentication invalid!`))
    //     }
    //
    //     // Authentication succeeded, save the context and proceed...
    //     req.user = user
    //     next()
    // })
}

/**
 *
 * @param req
 * @param res
 * @param next
 */

export async function adminRequired(req: Request, res: Response, next: NextFunction) {
    const {accounttype} = req.user;

    if (accounttype === "admin") {
        return next();
    }
    return res.sendStatus(Code.UNAUTHORIZED);
}

export const userAuthMiddlewares = [extractToken, authenticateToken];

export const adminAuthMiddlewares = [extractToken, authenticateToken, adminRequired];
