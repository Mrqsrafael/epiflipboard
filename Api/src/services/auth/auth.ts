import * as jwt from "jsonwebtoken";
import {User} from "../../types";

const jwtKey = process.env["JWT_KEY"];

export function generateAuthToken(user: User) {
    if (!jwtKey)
        return new Error("No Key found");
    return jwt.sign({id: user.id}, jwtKey);
}
