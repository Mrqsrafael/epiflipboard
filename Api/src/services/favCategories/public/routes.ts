import {Router} from "express";

import {favoriteCategory, getFavoriteCategories, getAllFavoriteCategories} from "./middlewares";
import {userAuthMiddlewares} from "../../auth/middlewares";

const router = Router();

router.post("", [...userAuthMiddlewares, favoriteCategory]);
router.get("", [...userAuthMiddlewares, getFavoriteCategories]);
router.get("/all", [getAllFavoriteCategories]);



// @ts-ignore
export default router;