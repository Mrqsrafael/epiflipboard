import request from "supertest";
import app from "../../../src/app";
import {getAll} from "../../../src/services/Magazine/controller";

describe("POST /magazine", () => {
    const data = {
        "name": "test"
    }
    it("should return 401 Unauthorize", () => {
        return request(app)
            .post("/magazine")
            .send(data)
            .expect(401);
    });
});

describe("POST /magazine", () => {
    const data = {
        // missing name
    }

    const token = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6NSwiaWF0IjoxNTk0MDc5NTg0fQ.MZk8bg3O3ic_mV9I1YOyagPcSUboo41fa6YvpVlSe2g"

    it("should return 400 BAD REQUEST", () => {
        return request(app)
            .post("/magazine")
            .set('Authorization', 'Bearer ' + token)
            .send(data)
            .expect(400);
    });
});

describe("POST /magazine", () => {
    const data = {
        "name": "test"
    }
    const token = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6NSwiaWF0IjoxNTk0MDc5NTg0fQ.MZk8bg3O3ic_mV9I1YOyagPcSUboo41fa6YvpVlSe2g"

    it("should return 200 OK", () => {
        return request(app)
            .post("/magazine")
            .set('Authorization', 'Bearer ' + token)
            .send(data)
            .expect(200);
    });
});


