import request from "supertest";
import app from "../../../src/app";


describe("DELETE /magazine", () => {
    const data = {
        // missing id
    }

    const token = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6NSwiaWF0IjoxNTk0MDc5NTg0fQ.MZk8bg3O3ic_mV9I1YOyagPcSUboo41fa6YvpVlSe2g"

    it("should return 401 BAD REQUEST", () => {
        return request(app)
            .delete("/magazine")
            .set('Authorization', 'Bearer ' + token)
            .send(data)
            .expect(400);
    });
});

describe("DELETE /magazine", () => {
    const data = {
        // missing id
    }
    it("should return 401 Unauthorize", () => {
        return request(app)
            .delete("/magazine")
            .send(data)
            .expect(401);
    });
});