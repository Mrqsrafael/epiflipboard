import request from "supertest";
import app from "../../../src/app";
import {getAll} from "../../../src/services/Magazine/controller";


describe("PUT /magazine", () => {
    const data = {
        "id": 15,
        // missing name
    }
    const token = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6NSwiaWF0IjoxNTk0MDc5NTg0fQ.MZk8bg3O3ic_mV9I1YOyagPcSUboo41fa6YvpVlSe2g"

    it("should return 401 BAD REQUEST", () => {
        return request(app)
            .put("/magazine")
            .set('Authorization', 'Bearer ' + token)
            .send(data)
            .expect(400);
    });
});

describe("PUT /magazine", () => {
    const data = {
        "id": 15,
        "name": "newName"
    }
    const token = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6NSwiaWF0IjoxNTk0MDc5NTg0fQ.MZk8bg3O3ic_mV9I1YOyagPcSUboo41fa6YvpVlSe2g"

    it("should return 200 OK", () => {
        return request(app)
            .put("/magazine")
            .set('Authorization', 'Bearer ' + token)
            .send(data)
            .expect(200);
    });
});


describe("PUT /magazine/addNews", () => {
    const data = {
        //missing data
    }
    const token = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6NSwiaWF0IjoxNTk0MDc5NTg0fQ.MZk8bg3O3ic_mV9I1YOyagPcSUboo41fa6YvpVlSe2g"

    it("should return 401 BAD REQUEST", () => {
        return request(app)
            .put("/magazine/addNews")
            .set('Authorization', 'Bearer ' + token)
            .send(data)
            .expect(400);
    });
});

describe("PUT /magazine/addNews", () => {
    const data = {
        "newsID": 19,
        "magazineID": 15
    }
    const token = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6NSwiaWF0IjoxNTk0MDc5NTg0fQ.MZk8bg3O3ic_mV9I1YOyagPcSUboo41fa6YvpVlSe2g"

    it("should return 200 OK", () => {
        return request(app)
            .put("/magazine/addNews")
            .set('Authorization', 'Bearer ' + token)
            .send(data)
            .expect(200);
    });
});

describe("PUT /magazine/removeNews", () => {
    const data = {
        //missing data
    }
    const token = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6NSwiaWF0IjoxNTk0MDc5NTg0fQ.MZk8bg3O3ic_mV9I1YOyagPcSUboo41fa6YvpVlSe2g"

    it("should return 401 BAD REQUEST", () => {
        return request(app)
            .put("/magazine/removeNews")
            .set('Authorization', 'Bearer ' + token)
            .send(data)
            .expect(400);
    });
});

describe("PUT /magazine/removeNews", () => {
    const data = {
        "newsID": 19,
        "magazineID": 15
    }
    const token = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6NSwiaWF0IjoxNTk0MDc5NTg0fQ.MZk8bg3O3ic_mV9I1YOyagPcSUboo41fa6YvpVlSe2g"

    it("should return 200 OK", () => {
        return request(app)
            .put("/magazine/removeNews")
            .set('Authorization', 'Bearer ' + token)
            .send(data)
            .expect(200);
    });
});

/*
describe("DELETE /magazine", async () => {
    const magazines = await getAll(5);

    const data = {
        "id": magazines[magazines.length - 1].id,
    }
    const token = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6NSwiaWF0IjoxNTk0MDc5NTg0fQ.MZk8bg3O3ic_mV9I1YOyagPcSUboo41fa6YvpVlSe2g"

    it("should return 200 OK", () => {
        return request(app)
            .delete("/magazine")
            .set('Authorization', 'Bearer ' + token)
            .send(data)
            .expect(200);
    });
});*/
